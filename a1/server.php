<?php

session_start();

class UserList {
	public function add($email, $password){
		$newTask = (object)[
			'email' => $email,
			'password' => $password,
		];

		if($_SESSION['users'] === null){
			$_SESSION['users'] = array();
		}

		array_push($_SESSION['users'], $newUser);
	}

	public function update($id, $email, $password){
		$_SESSION['users'][$id]->email = $email;
		$_SESSION['users'][$id]->password = ($password !== null) ? true : false;
	}

}

$userList = new UserList();

if($_POST['action'] === 'add'){
	$userList->add($_POST['email'], $_POST['password']);
} elseif ($_POST['action'] === 'update'){
	$userList->update($_POST['id'], $_POST['email'], $_POST['password']);
}

header('Location: ./index.php');